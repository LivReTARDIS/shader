Shader "Dancing GAYDER" {
   
     Properties {
         _MaxColorDistance ("Point of no more color", Int) = 1000
        
        _Girditemsize("Grid Item size",int) = 50
 
     } 
   SubShader { 
      Tags { "Queue" = "Transparent" } 
         // draw after all opaque geometry has been drawn
      Pass {
         

            ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
      
         CGPROGRAM 
 
         #pragma vertex vert  
         #pragma fragment frag
         #include "UnityCG.cginc" 

         fixed _MaxColorDistance;
         static const int speed = 20;

         static  const float pi = 3.141592653589793238462;
          static const float pi2 = 6.283185307179586476924;

         struct vertexInput {
            float3 vertex : POSITION;
            float4 tangent : TANGENT;  
            float3 normal : NORMAL;
            float4 texcoord : TEXCOORD0;  
            float4 texcoord1 : TEXCOORD1; 
            float4 texcoord2 : TEXCOORD2;  
            float4 texcoord3 : TEXCOORD3; 
            fixed4 color : COLOR;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 col : TEXCOORD0;
            float4 worldPos : TEXCOORD2;
            float3 viewDir : TEXCOORD3;
             float3 worldNormal : TEXCOORD4;

         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;

            output.worldPos = mul(unity_ObjectToWorld, input.vertex);
            output.worldNormal = UnityObjectToWorldNormal(input.normal.xyz);
            output.viewDir =  normalize(UnityWorldSpaceViewDir(output.worldPos));
            
            output.pos = UnityObjectToClipPos(input.vertex*1.07);
            output.col = float4(1,1,1,0.7);

            return output;
         }

         float4 frag(vertexOutput input) : SV_Target
         {

            float camDist = distance(input.worldPos, _WorldSpaceCameraPos);
            if(camDist< _MaxColorDistance)
               return input.col;
            return float4(0,0,0,0);
            
         }
 
         ENDCG  
          
         
          }
      Pass{
         ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
      
         CGPROGRAM 
 
         #pragma vertex vert  
         #pragma fragment frag
         #include "UnityCG.cginc" 

         fixed _MaxColorDistance;
         static const int speed = 20;

         static  const float pi = 3.141592653589793238462;
          static const float pi2 = 6.283185307179586476924;

         struct vertexInput {
            float3 vertex : POSITION;
            float4 tangent : TANGENT;  
            float3 normal : NORMAL;
            float4 texcooord : TEXCOORD0;  
            float4 texcoord1 : TEXCOORD1; 
            float4 texcoord2 : TEXCOORD2;  
            float4 texcoord3 : TEXCOORD3; 
            fixed4 color : COLOR;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 col : TEXCOORD0;
            float4 worldPos : TEXCOORD2;
            float3 viewDir : TEXCOORD3;
            float3 worldNormal : TEXCOORD4;
            float3 objectPos : TEXCOORD5;
            

         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;

            output.worldPos = mul(unity_ObjectToWorld, input.vertex);
            output.worldNormal = UnityObjectToWorldNormal(input.normal.xyz);
            output.viewDir =  normalize(UnityWorldSpaceViewDir(output.worldPos));
            output.objectPos = input.vertex;

            output.col.r = max(0,0.52+0.5*sin((_Time*speed) * pi ));
            output.col.g = max(0,0.52+0.5*sin((_Time*speed) * pi +2 ));
            output.col.b = max(0,0.52+0.5*sin((_Time*speed) * pi +4 ));
            output.col.a = 1;
            
            output.pos = UnityObjectToClipPos(input.vertex);
            


            return output;
         }

         //float2 position = -aspect.xy + 2.0 * gl_FragCoord.xy / resolution.xy * aspect.xy;
    float angle = 0.0 ;
    float radius = length(30) ;
    /*if (position.x != 0.0 && position.y != 0.0){
        angle = degrees(atan(position.y,position.x)) ;
    }
    float amod = mod(angle+30.0*time-120.0*log(radius), 30.0) ;
    if (amod<15.0){
        gl_FragColor = vec4( 0.0, 0.0, 0.0, 1.0 );
    } else{
        gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 );                    
    }
}
          *
          *
          * 
          */

                         float4 frag( vertexOutput input) : COLOR

         {

                     float camDist = distance(input.worldPos, _WorldSpaceCameraPos);
                     float percentGray = min(1,camDist/_MaxColorDistance);
                
                    float2 vec = input.worldPos.xy - float2(0.5, 0.5);
 
                    float l = length(vec/100);
                    float r = atan2(vec.y, vec.x) + pi; // 0-2π
                    float t = _Time.y*10;
                    float c = 1-sin(l*72+r+t);
                    c = step(0.5, c);

                    fixed4 rawC;
                            
                    if(c == 1)
                    {
                         rawC.r= max(0,0.52+0.5*sin((_Time*speed*1) * pi +1));
                         rawC.g = max(0,0.52+0.5*sin((_Time*speed*1) * pi +2 ));
                         rawC.b = max(0,0.52+0.5*sin((_Time*speed*1) * pi  ));
                       
                    }
                     else
                     {
                            rawC = float4(input.pos.r,1,0.4,1);           
                        
                        
                     }
                    float3 rgb = float3(c,c,c);

                                  fixed4 color;
            color = rawC*(1-percentGray) +float4(0.5,0.5,0.5,1)*percentGray;
             color.a = 1.4-dot(normalize(input.viewDir),normalize(input.worldNormal));
            return  color;
                

                         }
         
       
 
         ENDCG  
      }
   }
}