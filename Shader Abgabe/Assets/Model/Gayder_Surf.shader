Shader "Custom/Gayder_Surf"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
          _MaxColorDistance ("Strongest color at", Int) = 10
        _NoMoreColor ("Color Fading Border", Int) = 100

    }
    SubShader
    {
        Tags {  "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
    "Queue" = "Transparent"}
        LOD 200
        //ZWrite Off
       Blend SrcAlpha OneMinusSrcAlpha 

        CGPROGRAM
        
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha decal:blend 
        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        fixed _MaxColorDistance;
        fixed _NoMoreColor;

        static const int speed = 20;
        static  const float pi = 3.141592653589793238462;

        struct Input
        {
       
            float3 worldPos;
            float3 worldNormal;
            float3 viewDir;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;


        void surf (Input input, inout SurfaceOutputStandard o)
        {
                     
             fixed4 c = (1,1,1,1);
             fixed worldPos = mul(unity_ObjectToWorld, input.worldPos);
             float camDist = distance(input.worldPos, _WorldSpaceCameraPos);                          

            c.r = max(0,0.52+0.5*sin((_Time*speed) * pi ));
            c.g = max(0,0.52+0.5*sin((_Time*speed) * pi +2 ));
            c.b = max(0,0.52+0.5*sin((_Time*speed) * pi +4 ));
    
           float percentGray = min(1,abs(camDist-_MaxColorDistance)/_NoMoreColor);

            // Makes borders thicker
            c.a = 1.7-dot(normalize(input.viewDir),normalize(input.worldNormal));
            
            c = c*(1-percentGray) +float4(0.5,0.5,0.5,1)*percentGray;
            // Albedo comes from a texture tinted by color
            
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    
    FallBack "Diffuse"
}
