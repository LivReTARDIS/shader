Shader "CG GAYDER" {
   
     Properties {
         _MaxColorDistance ("Point of no more color", Int) = 1000
   
     } 
   SubShader { 
      Tags { "Queue" = "Transparent" } 
         // draw after all opaque geometry has been drawn
      Pass {
         

            ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
      
         CGPROGRAM 
 
         #pragma vertex vert  
         #pragma fragment frag
         #include "UnityCG.cginc" 

         fixed _MaxColorDistance;
         static const int speed = 20;

         static  const float pi = 3.141592653589793238462;
          static const float pi2 = 6.283185307179586476924;

         struct vertexInput {
            float3 vertex : POSITION;
            float4 tangent : TANGENT;  
            float3 normal : NORMAL;
            float4 texcoord : TEXCOORD0;  
            float4 texcoord1 : TEXCOORD1; 
            float4 texcoord2 : TEXCOORD2;  
            float4 texcoord3 : TEXCOORD3; 
            fixed4 color : COLOR;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 col : TEXCOORD0;
            float4 worldPos : TEXCOORD2;
            float3 viewDir : TEXCOORD3;
             float3 worldNormal : TEXCOORD4;

         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;

            output.worldPos = mul(unity_ObjectToWorld, input.vertex);
            output.worldNormal = UnityObjectToWorldNormal(input.normal.xyz);
            output.viewDir =  normalize(UnityWorldSpaceViewDir(output.worldPos));
            
            output.pos = UnityObjectToClipPos(input.vertex*1.07);
            output.col = float4(1,1,1,0.7);

            return output;
         }

         float4 frag(vertexOutput input) : SV_Target
         {

            float camDist = distance(input.worldPos, _WorldSpaceCameraPos);
            if(camDist< _MaxColorDistance)
               return input.col;
            return float4(0,0,0,0);
            
         }
 
         ENDCG  
          
         
          }
      Pass{
         ZWrite Off // don't write to depth buffer 
            // in order not to occlude other objects

         Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
      
         CGPROGRAM 
 
         #pragma vertex vert  
         #pragma fragment frag
         #include "UnityCG.cginc" 

         fixed _MaxColorDistance;
         static const int speed = 20;

         static  const float pi = 3.141592653589793238462;
          static const float pi2 = 6.283185307179586476924;

         struct vertexInput {
            float3 vertex : POSITION;
            float4 tangent : TANGENT;  
            float3 normal : NORMAL;
            float4 texcoord : TEXCOORD0;  
            float4 texcoord1 : TEXCOORD1; 
            float4 texcoord2 : TEXCOORD2;  
            float4 texcoord3 : TEXCOORD3; 
            fixed4 color : COLOR;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 col : TEXCOORD0;
            float4 worldPos : TEXCOORD2;
            float3 viewDir : TEXCOORD3;
             float3 worldNormal : TEXCOORD4;

         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;

            output.worldPos = mul(unity_ObjectToWorld, input.vertex);
            output.worldNormal = UnityObjectToWorldNormal(input.normal.xyz);
            output.viewDir =  normalize(UnityWorldSpaceViewDir(output.worldPos));

            output.col.r = max(0,0.52+0.5*sin((_Time*speed) * pi ));
            output.col.g = max(0,0.52+0.5*sin((_Time*speed) * pi +2 ));
            output.col.b = max(0,0.52+0.5*sin((_Time*speed) * pi +4 ));
            output.col.a = 1;
            
            output.pos = UnityObjectToClipPos(input.vertex);
            


            return output;
         }

         float4 frag(vertexOutput input) : COLOR 
         {
            float camDist = distance(input.worldPos, _WorldSpaceCameraPos);
            float percentGray = min(1,camDist/_MaxColorDistance);
            
           fixed4 c;
            c = input.col*(1-percentGray) +float4(0.5,0.5,0.5,1)*percentGray;
             c.a = 1.4-dot(normalize(input.viewDir),normalize(input.worldNormal));
            return  c;
         }
 
         ENDCG  
      }
   }
}